Feature: Test Calculator Functionality


  Scenario: Exercise 2
    Given Exercise 2 submodule is run
    When I input "2" and "14" and "5" and "15"
    Then I get result "15"


  Scenario: Exercise 22
    Given Exercise 22 submodule is run
    When I input "4"
    Then I get result "12"


  Scenario: Integration
    Given Integration submodule is run
    When I input "2*x" and "x" to IntegralCalculator
    Then I get result "x**2" for IntegralCalculator


  Scenario: Calculator
    Given Calculator submodule is run
    When I input "2+2" to Calculator submodule
    Then I get result "4" for Calculator submodule